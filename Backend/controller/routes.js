const express = require("express");
const multer = require("multer");
const getter = require("./getcontroller");
const helper = require("./postcontroller");
const fetch=require("node-fetch")
const router = express.Router();
router.route("/").get(getter.getindex);
router.route("/Register").get(getter.getregister);
router.route("/getblogs").get(getter.getblogs);
router.route("/getingblogs").post(getter.getingblogs);
router.route("/gettoblogpage").post(helper.savedata);
router.route("/index").post(helper.checkdata);
module.exports=router;